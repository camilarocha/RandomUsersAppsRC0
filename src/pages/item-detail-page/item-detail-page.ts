import { Component } from '@angular/core';
import { NavController, NavParams , ViewController  } from 'ionic-angular';

@Component({
  selector: 'page-item-detail-page',
  templateUrl: 'item-detail-page.html'
})
export class ItemDetailPage {

  private user: any = {
    picture: {},
    name: {},
    location: {},
    id: {}
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public view: ViewController) { }

  ionViewDidLoad() {
    let selectedItem = this.navParams.get('selectedItem');

    this.user = selectedItem;
  }

  close() {
    this.view.dismiss();
  }

}


